import colour
import numpy as np


def steprange(
    start: float | None = None,
    end: float | None = None,
    step: float = 0.0,
    length: int = 0,
) -> np.ndarray:
    if end is not None:
        return np.arange(end - (length * step), end, step)
    if start is not None:
        return np.arange(start + step, start + ((length + 1) * step), step)
    msg = "Start and end cannot be both None"
    raise TypeError(msg)


shp47 = colour.SpectralShape(400, 700, 1)
shp3678 = colour.SpectralShape(360, 780, 5)
shp475 = colour.SpectralShape(400, 750, 10)
CCT_D65 = 6500 * 1.4388 / 1.4380
xy = colour.temperature.CCT_to_xy_CIE_D(CCT_D65)
sd_d65 = colour.sd_CIE_illuminant_D_series(xy)
sd_d65.align(shp47)
xy2 = colour.temperature.CCT_to_xy_CIE_D(5000)
sd_d50 = colour.sd_CIE_illuminant_D_series(xy2)
sd_d50.align(shp475)
sd_a = colour.sd_CIE_standard_illuminant_A(shape=shp475)
sd1 = colour.sd_ones(shp47)
CIERGB = (
    np.array([[0.49, 0.31, 0.2], [0.17697, 0.81240, 0.01063], [0, 0.01, 0.99]])
    / 0.17697
)
srgb = np.array(
    [
        [0.41239080, 0.35758434, 0.18048079],
        [0.21263901, 0.71516868, 0.07219232],
        [0.01933082, 0.11919478, 0.95053215],
    ],
)
status_a_product = {
    "wavelength": np.arange(400, 760, 10),
    "blue": np.concatenate(
        (
            steprange(end=3.602, step=3.8, length=2),
            np.array([3.602, 4.819, 5.0, 4.912, 4.620, 4.040, 2.989, 1.566, 0.165]),
            steprange(start=1.650, step=-1.4, length=25),
        ),
    ),
    "green": np.concatenate(
        (
            steprange(end=1.650, step=2.2, length=10),
            np.array(
                [1.650, 3.822, 4.782, 5, 4.906, 4.644, 4.221, 3.609, 2.766, 1.579],
            ),
            steprange(start=1.579, step=-1.7, length=16),
        ),
    ),
    "red": np.concatenate(
        (
            steprange(end=2.568, step=2.7, length=20),
            np.array(
                [
                    2.568,
                    4.638,
                    5,
                    4.871,
                    4.604,
                    4.286,
                    3.9,
                    3.551,
                    3.165,
                    2.776,
                    2.383,
                    1.970,
                    1.551,
                    1.141,
                    0.741,
                    0.341,
                ],
            ),
            steprange(start=0.341, step=-0.4, length=0),
        ),
    ),
}
status_a = {
    c: (lambda x: x / (np.max(x)))(  # noqa: PLC3002
        10 ** status_a_product[c] / sd_a.values,
    )
    for c in ["red", "green", "blue"]
}
