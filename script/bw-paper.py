#!/usr/bin/env python3

import argparse
import json
import logging
from pathlib import Path

import pandas as pd
from plotly.io._base_renderers import open_html_in_browser

from traces.bw_trace import BWTrace

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def main(output: str, directory: str, *, plot: bool, report: bool) -> None:
    curve = pd.read_csv(Path(directory) / "dh.csv", index_col=0)["density"]
    trace = BWTrace(trace=curve)
    if plot:
        open_html_in_browser(trace.plot(directory))
    paper_data = trace.to_data()
    if report:
        open_html_in_browser(paper_data.report("bw", directory))
    paper_dict = paper_data.to_dict()
    with Path(output).open("w") as json_file:
        json.dump(paper_dict, json_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process fixed grade DH curves.")
    parser.add_argument("-o", help="Output paper description json")
    parser.add_argument("--plot", help="Plot loaded traces.", action="store_true")
    parser.add_argument(
        "--report",
        help="Compile a report with computed paper data.",
        action="store_true",
    )
    parser.add_argument("dir", help="Directory with the dh.csv files.")
    args = parser.parse_args()

    logging.info("Processing %s", args.dir)
    main(args.o, args.dir, plot=args.plot, report=args.report)
