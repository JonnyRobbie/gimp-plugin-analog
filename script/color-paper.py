#!/usr/bin/env python3

import argparse
import csv
import json
import logging
import math
from pathlib import Path

import numpy as np
import plotly.graph_objects as go
import yaml
from plotly.io._base_renderers import open_html_in_browser

from paper.color import Colour, CurvesTrace
from paper.color_curves import color_curves
from paper.color_density import ColorDensity
from paper.color_plotting import color_plot
from paper.color_sensitivity import ColorSensitivity
from paper.plotly_graphs import (
    create_color_dh,
    create_dens_curves,
    create_density_swatches,
    create_sens_curves,
)

logging.basicConfig(level=logging.DEBUG)


def load_sensitivity(datasheet: Path) -> CurvesTrace:
    sensitivity: CurvesTrace = {
        "red": {"wavelength": [], "sensitivity": []},
        "green": {"wavelength": [], "sensitivity": []},
        "blue": {"wavelength": [], "sensitivity": []},
    }

    for c in sensitivity:
        with Path(datasheet / ("spectrum-" + c + ".csv")).open(newline="") as csvfile:
            reader = csv.reader(csvfile)
            next(reader)  # skip header
            for row in reader:
                sensitivity[c]["wavelength"].append(float(row[0]))
                sensitivity[c]["sensitivity"].append(float(row[1]))

    return sensitivity


def load_curves(datasheet: Path) -> CurvesTrace:
    dh: CurvesTrace = {
        "red": {"exposure": [], "density": []},
        "green": {"exposure": [], "density": []},
        "blue": {"exposure": [], "density": []},
    }

    for c in dh:
        with Path(datasheet / ("dh-" + c + ".csv")).open(newline="") as csvfile:
            reader = csv.reader(csvfile)
            next(reader)  # skip header
            for row in reader:
                dh[c]["exposure"].append(float(row[0]))
                dh[c]["density"].append(float(row[1]))

    return dh


def load_density(datasheet: Path) -> CurvesTrace:
    density: CurvesTrace = {
        "cyan": {"wavelength": [], "density": []},
        "magenta": {"wavelength": [], "density": []},
        "yellow": {"wavelength": [], "density": []},
    }

    for c in density:
        with Path(datasheet / ("spectrum-" + c + ".csv")).open(newline="") as csvfile:
            reader = csv.reader(csvfile)
            next(reader)  # skip header
            for row in reader:
                density[c]["wavelength"].append(float(row[0]))
                density[c]["density"].append(float(row[1]))
    return density


def create_html(
    sens: go.Figure,
    dh: go.Figure,
    dens: go.Figure,
    sw1: go.Figure,
    sw2: go.Figure,
    sw_max: go.Figure,
    paper_name: str,
) -> str:
    return f"""
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <script src="https://cdn.plot.ly/plotly-2.20.0.min.js" charset="utf-8"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <h4>{paper_name}</h4>
        </div>
        <div class="row">
            <div class="col-8">{sens.to_html(include_plotlyjs=False, full_html=False)}</div>
            <div class="col-4">{sw1.to_html(include_plotlyjs=False, full_html=False)}</div>
        </div>
        <div class="row">
            <div class="col-8">{dh.to_html(include_plotlyjs=False, full_html=False)}</div>
            <div class="col-4">{sw2.to_html(include_plotlyjs=False, full_html=False)}</div>
        </div>
        <div class="row">
            <div class="col-8">{dens.to_html(include_plotlyjs=False, full_html=False)}</div>
            <div class="col-4">{sw_max.to_html(include_plotlyjs=False, full_html=False)}</div>
        </div>
    </div>
</body>
</html>
"""  # noqa: E501


def create_report(
    sensitivity: CurvesTrace,
    dh: CurvesTrace,
    density: CurvesTrace,
    paper_name: str = "",
) -> None:
    color_density = ColorDensity()
    cmy_dens = color_density.get_density(density)

    rep_sens = create_sens_curves(
        {
            c: [sensitivity[c]["wavelength"], sensitivity[c]["sensitivity"]]
            for c in ["red", "green", "blue"]
        },
    )
    rep_dens = create_dens_curves(
        {
            c: [density[c]["wavelength"], density[c]["density"]]
            for c in ["cyan", "magenta", "yellow"]
        },
    )
    rep_dh = create_color_dh(
        {c: [dh[c]["exposure"], dh[c]["density"]] for c in ["red", "green", "blue"]},
    )
    mask_rgb = [np.array([0, 1, 1]), np.array([1, 0, 1]), np.array([1, 1, 0])]
    mask_cmy = [np.array([0, 1, 0]), np.array([0, 0, 1]), np.array([1, 0, 0])]
    mask_k = [np.array([1, 1, 1])]
    # Create plot for density 1, density 2 and dmax
    dens_swatches = {
        f"Density {c_name}": [
            [
                Colour.from_xyz(
                    np.prod(
                        np.array(
                            [
                                np.power(c.xyz, d)
                                for c, d in zip(cmy_dens, d * mask, strict=False)
                            ],
                        ),
                        axis=0,
                    ),
                ).srgb
                for mask in masks
            ]
            for masks in [mask_cmy, mask_rgb, mask_k]
        ]
        for d, c_name in zip(
            [
                np.full(3, 1),
                np.full(3, 2),
                np.array(
                    [
                        max(dh["red"]["density"]),
                        max(dh["green"]["density"]),
                        max(dh["blue"]["density"]),
                    ],
                ),
            ],
            ["1", "2", "max"],
            strict=False,
        )
    }
    rep_sw_1, rep_sw_2, rep_sw_max = (
        create_density_swatches(patchset, title)
        for title, patchset in dens_swatches.items()
    )
    html_figs = create_html(
        rep_sens,
        rep_dh,
        rep_dens,
        rep_sw_1,
        rep_sw_2,
        rep_sw_max,
        paper_name,
    )
    open_html_in_browser(html_figs)


def main(directory: str, output: str, *, report: bool, plot: bool) -> None:
    # https://mina86.com/2019/srgb-xyz-conversion/
    datasheet = Path(directory)

    sensitivity = load_sensitivity(datasheet)
    dh = load_curves(datasheet)
    density = load_density(datasheet)

    color_sensitivity = ColorSensitivity()
    color_density = ColorDensity()

    xyz_sens = color_sensitivity.get_sensitivity(sensitivity)
    dh_x, dh_n = color_curves(dh)
    # convert cmy-like colors to log rgb-like for optimization
    # TODO(marek): check if the optimized is really used in the gegl
    xyz_dens = {
        name: [math.log10(1 / c) for c in triple.xyz]
        for triple, name in zip(
            color_density.get_density(density),
            ["cdens", "mdens", "ydens"],
            strict=False,
        )
    }

    paper_data = {**dh_x, **dh_n, **xyz_sens, **xyz_dens}

    with Path(output).open("w") as json_file:
        json.dump(paper_data, json_file)

    if report:
        with Path(datasheet / "info.yml").open() as f:
            paper_meta = yaml.safe_load(f)
        create_report(sensitivity, dh, density, paper_meta["desc"])

    if (
        plot
        and color_density.dye is not None
        and color_density.cmy_d1 is not None
        and color_sensitivity.sens_layer is not None
    ):
        color_plot(
            color_density.dye,
            density,
            color_density.cmy_d1,
            color_sensitivity.sens_layer,
            sensitivity,
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process color DH curves.")
    parser.add_argument("--plot", action="store_true", help="Plot the colors")
    parser.add_argument("--report", action="store_true", help="Create a report")
    parser.add_argument("-o", help="Output paper description json")
    parser.add_argument(
        "dir",
        help="Directory with the dh-red.csv, dh-green.csv, dh-blue.csv, "
        "spectrum-cyan.csv, spectrum-magenta.csv and spectrum-yellow.csv files.",
    )

    args = parser.parse_args()

    logging.info("Processing %s", args.dir)
    main(args.dir, args.o, report=args.report, plot=args.plot)
