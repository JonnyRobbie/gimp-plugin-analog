import numpy as np
import pandas as pd
import plotly.graph_objects as go

from html_templates import bw_report
from paper.color import Tricolour


class PaperData:
    sensitivity: np.ndarray
    red_curve: pd.Series
    green_curve: pd.Series
    blue_curve: pd.Series
    dye: Tricolour

    def __init__(
        self,
        sensitivity: np.ndarray,
        red: pd.Series,
        green: pd.Series,
        blue: pd.Series,
        dye: Tricolour,
    ) -> None:
        self.sensitivity = sensitivity
        self.red_curve = red
        self.green_curve = green
        self.blue_curve = blue
        self.dye = dye  # dyes at density 1

    def to_dict(self) -> dict[str, list[float] | int]:
        return {
            "rsens": self.sensitivity[0].tolist(),
            "gsens": self.sensitivity[1].tolist(),
            "bsens": self.sensitivity[2].tolist(),
            "rx": self.red_curve.index.tolist(),
            "ry": self.red_curve.tolist(),
            "rn": len(self.red_curve),
            "gx": self.green_curve.index.tolist(),
            "gy": self.green_curve.tolist(),
            "gn": len(self.green_curve),
            "bx": self.blue_curve.index.tolist(),
            "by": self.blue_curve.tolist(),
            "bn": len(self.blue_curve),
            "cdens": self.dye.a.density.tolist(),
            "mdens": self.dye.b.density.tolist(),
            "ydens": self.dye.c.density.tolist(),
        }

    def report(self, paper_type: str, paper_name: str) -> str:
        if paper_type == "bw":
            return self._report_bw(paper_name)
        if paper_type == "mg":
            return self._report_mg(paper_name)
        return ""

    def _report_bw(self, paper_name: str) -> str:
        dh = go.Figure()
        return bw_report(paper_name, dh)

    def _report_mg(self, paper_name: str) -> str:  # noqa: ARG002
        return ""
        # rx = [d - opt1.x[grades.index(plot) + 3] for d in dh["5"]["exposure"]]
        # ry = (opt2.x * opt1.x[0]).tolist()
        # gx = [
        #     d
        #     - (
        #         opt1.x[-1] * opt1.x[grades.index(plot) + 3]
        #         + (1 - opt1.x[-1]) * opt1.x[grades.index(plot) + 3 + n_gr]
        #     )
        #     for d in dh["5"]["exposure"]
        # ]
        # gy = (opt2.x * opt1.x[1]).tolist()
        # bx = [d - opt1.x[grades.index(plot) + 3 + n_gr] for d in dh["5"]["exposure"]]
        # by = (opt2.x * opt1.x[2]).tolist()

        # full_x = np.concatenate((rx, gx, bx))
        # full_x.sort()
        # full_y = np.interp(full_x, rx, ry)
        # full_y += np.interp(full_x, gx, gy)
        # full_y += np.interp(full_x, bx, by)
        #
        # plt.plot(rx, ry, color="red")
        # plt.plot(gx, gy, color="green")
        # plt.plot(bx, by, color="blue")
        #
        # for grade in grades:
        #     plt.plot(
        #         dh[grade]["exposure"],
        #         dh[grade]["density"],
        #         "--" if plot == grade else ":",
        #         color="gray",
        #         label=grade,
        #     )
        # plt.plot(full_x, full_y, color="black")
