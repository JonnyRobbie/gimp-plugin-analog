import plotly.graph_objects as go


def _html_boilerplate(body: str) -> str:
    return f"""
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <script src="https://cdn.plot.ly/plotly-2.20.0.min.js" charset="utf-8"></script>
</head>
<body>
    <div class="container">
{body}
    </div>
</body>
</html>
"""  # noqa: E501


def bw_report(paper_name: str, dh: go.Figure) -> str:
    body = f"""
<div class="row">
    <h3>Trace plot/report for BW paper</h3>
    <h4>{paper_name}</h4>
</div>
<div class="row">
    <div class="col">{dh.to_html(include_plotlyjs=False, full_html=False)}</div>
</div>
    """
    return _html_boilerplate(body)


def mg_plot(paper_name: str, dh: go.Figure) -> str:
    body = f"""
<div class="row">
    <h3>Trace plots for multigrade paper</h3>
    <h4>{paper_name}</h4>
</div>
<div class="row">
    <div class="col">{dh.to_html(include_plotlyjs=False, full_html=False)}</div>
</div>
    """
    return _html_boilerplate(body)


def mg_report(paper_name: str, dh: go.Figure) -> str:
    body = f"""
<div class="row">
    <h3>Report for multigrade paper</h3>
    <h4>{paper_name}</h4>
</div>
<div class="row">
    <div class="col">{dh.to_html(include_plotlyjs=False, full_html=False)}</div>
</div>
    """
    return _html_boilerplate(body)


def color_report(
    sens: go.Figure,
    dh: go.Figure,
    dens: go.Figure,
    sw1: go.Figure,
    sw2: go.Figure,
    sw_max: go.Figure,
    paper_name: str,
) -> str:
    return f"""
<div class="row">
    <h4>{paper_name}</h4>
</div>
<div class="row">
    <div class="col-8">{sens.to_html(include_plotlyjs=False, full_html=False)}</div>
    <div class="col-4">{sw1.to_html(include_plotlyjs=False, full_html=False)}</div>
</div>
<div class="row">
    <div class="col-8">{dh.to_html(include_plotlyjs=False, full_html=False)}</div>
    <div class="col-4">{sw2.to_html(include_plotlyjs=False, full_html=False)}</div>
</div>
<div class="row">
    <div class="col-8">{dens.to_html(include_plotlyjs=False, full_html=False)}</div>
    <div class="col-4">{sw_max.to_html(include_plotlyjs=False, full_html=False)}</div>
    </div>
"""
