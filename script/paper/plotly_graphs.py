import logging
import math

import plotly.graph_objects as go

from .color import CurvesTraceList

logger = logging.getLogger(__name__)


def create_mg_grades(grades: dict[str, dict[str, list[list[float]]]]) -> go.Figure:
    fig = go.Figure()

    for k, v in grades.items():
        fig.add_trace(
            go.Scatter(
                x=v["orig"][0],
                y=v["orig"][1],
                mode="lines",
                line={"color": "black"},
                name=f"{k}",
            ),
        )
        fig.add_trace(
            go.Scatter(
                x=v["calc"][0],
                y=v["calc"][1],
                mode="lines",
                line={"color": "black", "dash": "dot"},
                name=f"{k}'",
            ),
        )
    fig.update_layout(title="Multigrade density gradation curves")

    return fig


def create_mg_layers(dh: list[list[list[float]]]) -> go.Figure:
    fig = go.Figure()
    for layer in dh:
        fig.add_trace(
            go.Scatter(x=layer[0], y=layer[1], mode="lines", line={"color": "black"}),
        )
    fig.update_layout(showlegend=False, title="Multigrade layer curves")

    return fig


def create_bw_dh(exp: list[float], dens: list[float]) -> go.Figure:
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=exp, y=dens, mode="lines", line={"color": "black"}))
    fig.update_layout(showlegend=False, title="Density curve")
    return fig


def create_color_dh(dh: CurvesTraceList) -> go.Figure:
    fig = go.Figure()
    for col in ["red", "green", "blue"]:
        fig.add_trace(
            go.Scatter(x=dh[col][0], y=dh[col][1], mode="lines", line={"color": col}),
        )
    fig.update_layout(showlegend=False, title="Density curves")
    return fig


def create_dens_curves(curves: CurvesTraceList) -> go.Figure:
    fig = go.Figure()
    for col in ["cyan", "magenta", "yellow"]:
        fig.add_trace(
            go.Scatter(
                x=curves[col][0],
                y=curves[col][1],
                mode="lines",
                line={"color": col},
            ),
        )
    fig.update_layout(showlegend=False, title="Spectral density curves")
    return fig


def create_sens_curves(curves: CurvesTraceList) -> go.Figure:
    fig = go.Figure()
    for col in ["red", "green", "blue"]:
        fig.add_trace(
            go.Scatter(
                x=curves[col][0],
                y=curves[col][1],
                mode="lines",
                line={"color": col},
            ),
        )
    fig.update_layout(showlegend=False, title="Spectral sensitivity curves")
    return fig


def create_sensitivity_swatches(patchsets: list[str]) -> go.Figure:
    fig = go.Figure()
    for t in range(3):
        x0 = 2 * math.sin(4 * (t / 6) * math.pi)
        y0 = 2 * math.cos(4 * (t / 6) * math.pi)
        r = go.layout.Shape(
            type="rect",
            x0=x0,
            x1=x0 + 2,
            y0=y0,
            y1=y0 + 2,
            line_color=patchsets[t],
            fillcolor=patchsets[t],
        )
        fig.add_shape(r)
    fig.update_layout(
        xaxis_range=[-2.5, 3.5],
        yaxis_range=[-2, 4],
        template="plotly_dark",
    )

    return fig


def create_density_swatches(patchset: list[list[str]], title: str) -> go.Figure:
    fig = go.Figure()
    for t in range(6):
        x0 = 2 * math.sin(2 * (t / 6) * math.pi)
        y0 = 2 * math.cos(2 * (t / 6) * math.pi)
        t_ = t % 2
        logger.debug("Col swatch %s: %s", t, patchset[t_][int(t / 2)])
        r = go.layout.Shape(
            type="rect",
            x0=x0,
            x1=x0 + 1,
            y0=y0,
            y1=y0 + 1,
            line_color=patchset[t_][int(t / 2)],
            fillcolor=patchset[t_][int(t / 2)],
        )
        fig.add_shape(r)
    logger.debug("Black swatch %s: %s", t, patchset[2][0])
    r = go.layout.Shape(
        type="rect",
        x0=0,
        x1=1,
        y0=0,
        y1=1,
        line_color=patchset[2][0],
        fillcolor=patchset[2][0],
    )
    fig.add_shape(r)
    fig.update_xaxes(range=[-3, 4])
    fig.update_yaxes(range=[-3, 4])
    fig.update_layout(template="plotly_white", title=title)
    return fig


if __name__ == "__main__":
    dens_fig = create_density_swatches(
        [
            ["#009090", "#900090", "#909000"],
            ["#900000", "#009000", "#000090"],
            ["#000000"],
        ],
        title="Density 1",
    )
    dens_fig.show()

    sens_fig = create_sensitivity_swatches(["#ff0000", "#00ff00", "#0000ff"])

    dh_fig = create_color_dh(
        {
            "red": [[0.5, 2.5], [0, 2]],
            "green": [[0.6, 2.6], [0, 2]],
            "blue": [[0.7, 2.7], [0, 2]],
        },
    )

    dc_fig = create_dens_curves(
        {
            "cyan": [[400, 500], [0, 1]],
            "magenta": [[450, 550, 650], [1, 0, 1]],
            "yellow": [[600, 700], [1, 0]],
        },
    )

    sc_fig = create_sens_curves(
        {
            "red": [[400, 500], [0, 1]],
            "green": [[450, 550, 650], [1, 0, 1]],
            "blue": [[600, 700], [1, 0]],
        },
    )

    bw_fig = create_bw_dh(exp=[0.5, 2.5], dens=[0, 2])

    mg_fig = create_mg_layers([[[0.5, 1.5], [0, 1]], [[1, 3], [0, 2]]])

    grade_fig = create_mg_grades(
        {
            "00": {"orig": [[0.5, 2.5], [0, 2]], "calc": [[0.6, 2.6], [0, 2]]},
            "2": {"orig": [[0.8, 2.2], [0, 2]], "calc": [[0.9, 2.2], [0.1, 2]]},
        },
    )
