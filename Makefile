all: negative-darkroom

negative-darkroom: operation/negative-darkroom/negative-darkroom-curve-enum.h operation/negative-darkroom.o
	-mkdir bin
	#cd operation; \
	#$(MAKE) all
	gcc -shared operation/negative-darkroom.o -o bin/negative-darkroom.so

operation/negative-darkroom.o: operation/negative-darkroom.c operation/config.h operation/negative-darkroom/negative-darkroom-curve-enum.c operation/negative-darkroom/negative-darkroom-curve-enum.h
	gcc -c -O3 -fpic $(shell pkg-config --cflags --libs gegl-0.4) -Ioperation -o operation/negative-darkroom.o operation/negative-darkroom.c

datasheet: datasheet/fuji-crystal/paper.json \
	datasheet/ilfobrom-1/paper.json \
	datasheet/ilfobrom-2/paper.json \
	datasheet/ilfobrom-3/paper.json \
	datasheet/ilfobrom-4/paper.json \
	datasheet/ilford-mg4/paper.json \
	datasheet/fomabrom-c/paper.json \
	datasheet/fomabrom-n/paper.json \
	datasheet/fomabrom-sp/paper.json \
	datasheet/fomabrom-s/paper.json \
	datasheet/fomabrom-variant/paper.json \
	datasheet/kodak-ultra/paper.json \
	datasheet/kodak-portra/paper.json \
	datasheet/kodak-supra/paper.json \
	datasheet/kodak-ektachrome/paper.json \

# Color papers

datasheet/fuji-crystal/paper.json: script/color-paper.py $(wildcard script/Paper/%.py) $(wildcard datasheet/fuji-crystal/%.csv)
	script/color-paper.py -o $@ datasheet/fuji-crystal
datasheet/kodak-ultra/paper.json: script/color-paper.py $(wildcard script/Paper/%.py) $(wildcard datasheet/kodak-ultra/%.csv)
	script/color-paper.py -o $@ datasheet/kodak-ultra
datasheet/kodak-portra/paper.json: script/color-paper.py $(wildcard script/Paper/%.py) $(wildcard datasheet/kodak-portra/%.csv)
	script/color-paper.py -o $@ datasheet/kodak-portra
datasheet/kodak-supra/paper.json: script/color-paper.py $(wildcard script/Paper/%.py) $(wildcard datasheet/kodak-supra/%.csv)
	script/color-paper.py -o $@ datasheet/kodak-supra
datasheet/kodak-ektachrome/paper.json: script/color-paper.py $(wildcard script/Paper/%.py) $(wildcard datasheet/kodak-ektachrome/%.csv)
	script/color-paper.py -o $@ datasheet/kodak-ektachrome

# Fixed contrast BW papers

datasheet/ilfobrom-1/paper.json: script/bw-paper.py $(wildcard datasheet/ilfobrom-1/%.csv)
	script/bw-paper.py -o $@ datasheet/ilfobrom-1

datasheet/ilfobrom-2/paper.json: script/bw-paper.py $(wildcard datasheet/ilfobrom-2/%.csv)
	script/bw-paper.py -o $@ datasheet/ilfobrom-2

datasheet/ilfobrom-3/paper.json: script/bw-paper.py $(wildcard datasheet/ilfobrom-3/%.csv)
	script/bw-paper.py -o $@ datasheet/ilfobrom-3

datasheet/ilfobrom-4/paper.json: script/bw-paper.py $(wildcard datasheet/ilfobrom-4/%.csv)
	script/bw-paper.py -o $@ datasheet/ilfobrom-4

datasheet/fomabrom-c/paper.json: script/bw-paper.py $(wildcard datasheet/fomabrom-c/%.csv)
	script/bw-paper.py -o $@ datasheet/fomabrom-c

datasheet/fomabrom-n/paper.json: script/bw-paper.py $(wildcard datasheet/fomabrom-n/%.csv)
	script/bw-paper.py -o $@ datasheet/fomabrom-n

datasheet/fomabrom-sp/paper.json: script/bw-paper.py $(wildcard datasheet/fomabrom-sp/%.csv)
	script/bw-paper.py -o $@ datasheet/fomabrom-sp

datasheet/fomabrom-s/paper.json: script/bw-paper.py $(wildcard datasheet/fomabrom-s/%.csv)
	script/bw-paper.py -o $@ datasheet/fomabrom-s

# Multigrade BW papers

datasheet/ilford-mg4/paper.json: script/mg-paper.py $(wildcard datasheet/ilford-mg4/%.csv)
	script/mg-paper.py -o $@ datasheet/ilford-mg4

datasheet/fomabrom-variant/paper.json: script/mg-paper.py $(wildcard datasheet/fomabrom-variant/%.csv)
	script/mg-paper.py -o $@ datasheet/fomabrom-variant

operation/negative-darkroom/negative-darkroom-curve-enum.c: \
datasheet/fuji-crystal/paper.json datasheet/fuji-crystal/info.yml \
datasheet/kodak-ultra/paper.json datasheet/kodak-ultra/info.yml \
datasheet/kodak-portra/paper.json datasheet/kodak-portra/info.yml \
datasheet/kodak-supra/paper.json datasheet/kodak-supra/info.yml \
datasheet/kodak-ektachrome/paper.json datasheet/kodak-ektachrome/info.yml \
datasheet/ilfobrom-1/paper.json datasheet/ilfobrom-1/info.yml \
datasheet/ilfobrom-2/paper.json datasheet/ilfobrom-2/info.yml \
datasheet/ilfobrom-3/paper.json datasheet/ilfobrom-3/info.yml \
datasheet/ilfobrom-4/paper.json datasheet/ilfobrom-4/info.yml \
datasheet/ilford-mg4/paper.json datasheet/ilford-mg4/info.yml \
datasheet/fomabrom-c/paper.json datasheet/fomabrom-c/info.yml \
datasheet/fomabrom-n/paper.json datasheet/fomabrom-n/info.yml \
datasheet/fomabrom-sp/paper.json datasheet/fomabrom-sp/info.yml \
datasheet/fomabrom-s/paper.json datasheet/fomabrom-s/info.yml \
datasheet/fomabrom-variant/paper.json datasheet/fomabrom-variant/info.yml
	mkdir -p operation/negative-darkroom
	script/gen-c.py --oc operation/negative-darkroom/negative-darkroom-curve-enum.c \
	  --oh operation/negative-darkroom/negative-darkroom-curve-enum.h \
		datasheet/kodak-ultra \
		datasheet/kodak-portra \
		datasheet/kodak-supra \
		datasheet/kodak-ektachrome \
		datasheet/fuji-crystal \
		datasheet/ilfobrom-1 \
		datasheet/ilfobrom-2 \
		datasheet/ilfobrom-3 \
		datasheet/ilfobrom-4 \
		datasheet/ilford-mg4 \
		datasheet/fomabrom-c \
		datasheet/fomabrom-n \
		datasheet/fomabrom-sp \
		datasheet/fomabrom-s \
		datasheet/fomabrom-variant

operation/negative-darkroom/negative-darkroom-curve-enum.h: operation/negative-darkroom/negative-darkroom-curve-enum.c

clean:
	-rm operation/*.o
	-rm operation/negative-darkroom/*
	-rmdir operation/negative-darkroom
	-rm bin/*
	-rmdir bin
	-rm datasheet/*/paper.json

