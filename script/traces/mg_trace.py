import logging

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import scipy as sp

from html_templates import mg_plot
from paper.color import Colour, Tricolour
from paper.paper_data import PaperData

logger = logging.getLogger(__name__)

REFERENCE_GRADE = "2"


class MGTrace:
    traces: dict[str, pd.Series]

    def __init__(self, traces: dict[str, pd.Series]) -> None:
        if any(grade not in traces for grade in (REFERENCE_GRADE, "5")):
            msg = "The datasheet must contain grades {REFERENCE_GRADE}' and 5."
            raise ValueError(msg)
        self.traces = traces

    def _optimize(self) -> tuple[dict[str, pd.Series], float]:  # noqa: PLR0915
        # dh = self.traces

        grades = list(self.traces)
        n_gr = len(self.traces)

        # We split the optimization into two phases: "shift" optimization and
        # latent curve optimization and loop those phases.
        # We do that under the assumption that there's gonna be a significant
        # shifts in the curves and the hope is that having one phase as "just" a
        # shift is more efficient that optimizing the whole curve end to end.

        # Prepare constraints for grade curves shift optimization
        bounds_grade_curves = [
            *(3 * [[0, 2]]),  # Emulsion density multpliers
            *(n_gr * [[-2, 1]]),  # Primary emulsion shift along grades
            *(n_gr * [[-1, 1]]),  # Secondary emulsion shift along grades
            [0, 1],  # Tertiary emulsion relative position
        ]
        constraints_grade_curves = []
        for factor in range(len(bounds_grade_curves)):
            lower, upper = bounds_grade_curves[factor]
            low = {"type": "ineq", "fun": lambda x, lb=lower, i=factor: x[i] - lb}
            u = {"type": "ineq", "fun": lambda x, ub=upper, i=factor: ub - x[i]}
            constraints_grade_curves.append(low)
            constraints_grade_curves.append(u)

        # Prepare constraints for latent curve shape optimization.
        bounds_latent_curve = [[0, 3]] * len(self.traces["5"])
        constraints_latent_curve = []
        for factor in range(len(bounds_latent_curve)):
            lower, upper = bounds_latent_curve[factor]
            low = {"type": "ineq", "fun": lambda x, lb=lower, i=factor: x[i] - lb}
            u = {"type": "ineq", "fun": lambda x, ub=upper, i=factor: ub - x[i]}
            constraints_latent_curve.append(low)
            constraints_latent_curve.append(u)

        # Constraint that the DH is monotonically increasing
        constraints_latent_curve += [
            {"type": "ineq", "fun": lambda x, j=i: x[j + 1] - x[j]}
            for i in range(len(self.traces["5"]) - 1)
        ]

        # Objective functions for both phases of optimization
        def grade_curves_objective(x: np.ndarray, prev: list[float]) -> float:
            # x[0]..x[2] Emulsion density multipliers
            # x[3]..x[9] Primary emulsion shift along grades
            # x[10]..x[16] Secondary emulsion shift along grades
            # x[17] Tertiary emulstion relative position

            # est_exp = np.array(dh["5"]["exposure"])
            est_exp = self.traces["5"].index.to_numpy()
            est_dens = np.array(prev)

            def curve_sum(grade: str) -> float:
                idx = grades.index(grade)
                g_dens = self.traces[grade].to_numpy()
                g_exp = self.traces[grade].index.to_numpy()
                red = np.interp(g_exp, est_exp - x[3 + idx], est_dens * x[0])
                green = np.interp(
                    g_exp,
                    est_exp - (x[-1] * x[3 + idx] + (1 - x[-1]) * x[3 + n_gr + idx]),
                    est_dens * x[1],
                )
                blue = np.interp(g_exp, est_exp - x[3 + n_gr + idx], est_dens * x[2])
                esti = red + green + blue
                return ((esti - g_dens) ** 2).mean()

            # SSE
            return sum([curve_sum(grade) for grade in grades])

        def latent_curve_objective(x: np.ndarray, prev: list[float]) -> float:
            est_exp = self.traces["5"].index.to_numpy()
            est_dens = np.array(x)

            # # Smooth it out
            def curve_sum(grade: str) -> float:
                idx = grades.index(grade)
                g_dens = self.traces[grade].to_numpy()
                g_exp = self.traces[grade].index.to_numpy()
                red = np.interp(g_exp, est_exp - prev[3 + idx], est_dens * prev[0])
                green = np.interp(
                    g_exp,
                    est_exp
                    - (
                        prev[-1] * prev[3 + idx] + (1 - prev[-1]) * prev[3 + n_gr + idx]
                    ),
                    est_dens * prev[1],
                )
                blue = np.interp(
                    g_exp,
                    est_exp - prev[3 + n_gr + idx],
                    est_dens * prev[2],
                )
                esti = red + green + blue
                return ((esti - g_dens) ** 2).mean()

            # SSE
            return sum([curve_sum(grade) for grade in grades])

        current_grade_curves = np.concatenate(
            (
                np.array([1 / 3, 1 / 3, 1 / 3]),
                np.linspace(start=1, stop=0, num=n_gr),
                np.array(n_gr * [0]),
                np.array([0.5]),
            ),
        )
        current_latent_curve = self.traces["5"].to_numpy()

        # Repeat the grade/latent curve optimization loop until it reaches equilibrium
        for x in range(10):
            grade_curves_optimization = sp.optimize.minimize(
                grade_curves_objective,
                current_grade_curves,
                args=current_latent_curve,
                method="COBYLA",
                constraints=constraints_grade_curves,
                options={"maxiter": 10000},
            )
            current_grade_curves = grade_curves_optimization.x

            latent_curve_optimization = sp.optimize.minimize(
                latent_curve_objective,
                current_latent_curve,
                args=current_grade_curves,
                method="COBYLA",
                constraints=constraints_latent_curve,
                options={"maxiter": 10000, "disp": False},
            )
            current_latent_curve = latent_curve_optimization.x
            logger.debug(
                "Loop %s; grade in %s its, f: %s; latent in %s its, f: %s",
                x,
                grade_curves_optimization.nfev,
                grade_curves_optimization.fun,
                latent_curve_optimization.nfev,
                latent_curve_optimization.fun,
            )

        # Smooth it out
        current_latent_curve = np.pad(current_latent_curve, (1, 1), mode="edge")
        current_latent_curve = np.convolve(
            current_latent_curve,
            [0.3, 0.4, 0.3],
            "valid",
        )

        red_series = pd.Series(
            current_latent_curve * current_grade_curves[0],
            index=[
                d - grade_curves_optimization.x[grades.index(REFERENCE_GRADE) + 3]
                for d in self.traces["5"].index
            ],
        )
        green_series = pd.Series(
            current_latent_curve * current_grade_curves[1],
            index=[
                d
                - (
                    current_grade_curves[-1]
                    * current_grade_curves[grades.index(REFERENCE_GRADE) + 3]
                    + (1 - current_grade_curves[-1])
                    * current_grade_curves[grades.index(REFERENCE_GRADE) + 3 + n_gr]
                )
                for d in self.traces["5"].index
            ],
        )
        blue_series = pd.Series(
            current_latent_curve * current_grade_curves[2],
            index=[
                d - current_grade_curves[grades.index(REFERENCE_GRADE) + 3 + n_gr]
                for d in self.traces["5"].index
            ],
        )

        return {
            "red": red_series,
            "green": green_series,
            "blue": blue_series,
        }, current_grade_curves[-1]

    def to_data(self) -> PaperData:
        curves, shift = self._optimize()

        return PaperData(
            sensitivity=np.array(((0.0, 0.0, 0.0), (0.0, shift, 1.0), (0.0, 1.0, 1.0))),
            red=curves["red"],
            green=curves["green"],
            blue=curves["blue"],
            dye=Tricolour(
                Colour(0.1, 0.1, 0.1),
                Colour(0.1, 0.1, 0.1),
                Colour(0.1, 0.1, 0.1),
            ),
        )

    def plot(self, trace_name: str) -> str:
        dh = go.Figure()
        return mg_plot(trace_name, dh)
