#!/usr/bin/env python3

import argparse
import json
from pathlib import Path

import yaml

parser = argparse.ArgumentParser(description="Generate .c and .h code.")
parser.add_argument("--oc", required=True, help="Output C file.")
parser.add_argument("--oh", required=True, help="Output H file.")
parser.add_argument(
    "dir",
    nargs="+",
    help="Directory with the info.yml and paper.json files.",
)

args = parser.parse_args()

with Path(args.oc).open("w") as cfile, Path(args.oh).open("w") as header:
    cfile.write("enum_start(neg_curve)\n")
    header.write(f"static HDCurve curves[{len(args.dir)}] = {{\n")
    for path in args.dir:
        with Path(Path(path) / "info.yml").open() as yml, Path(
            Path(path) / "paper.json",
        ).open() as jsn:
            paper_yml = yaml.safe_load(yml)
            cfile.write(
                '\tenum_value ({}, "{}", N_("{}"))\n'.format(
                    paper_yml["enum"],
                    paper_yml["name"],
                    paper_yml["desc"],
                ),
            )

            paper = json.load(jsn)
            header.write("{\n")
            header.write("// {}\n".format(paper_yml["desc"]))

            header.write(
                ".rx = (gfloat[]){{{}}},\n".format(", ".join(map(str, paper["rx"]))),
            )
            header.write(
                ".ry = (gfloat[]){{{}}},\n".format(", ".join(map(str, paper["ry"]))),
            )
            header.write(".rn = {},\n".format(str(len(paper["rx"]))))

            header.write(
                ".gx = (gfloat[]){{{}}},\n".format(", ".join(map(str, paper["gx"]))),
            )
            header.write(
                ".gy = (gfloat[]){{{}}},\n".format(", ".join(map(str, paper["gy"]))),
            )
            header.write(".gn = {},\n".format(str(len(paper["gx"]))))

            header.write(
                ".bx = (gfloat[]){{{}}},\n".format(", ".join(map(str, paper["bx"]))),
            )
            header.write(
                ".by = (gfloat[]){{{}}},\n".format(", ".join(map(str, paper["by"]))),
            )
            header.write(".bn = {},\n".format(str(len(paper["bx"]))))

            header.write(
                ".rsens = {{{}}},\n".format(", ".join(map(str, paper["rsens"]))),
            )
            header.write(
                ".gsens = {{{}}},\n".format(", ".join(map(str, paper["gsens"]))),
            )
            header.write(
                ".bsens = {{{}}},\n".format(", ".join(map(str, paper["bsens"]))),
            )

            header.write(
                ".cdens = {{{}}},\n".format(", ".join(map(str, paper["cdens"]))),
            )
            header.write(
                ".mdens = {{{}}},\n".format(", ".join(map(str, paper["mdens"]))),
            )
            header.write(
                ".ydens = {{{}}}\n".format(", ".join(map(str, paper["ydens"]))),
            )
            header.write("}}{}\n".format("" if args.dir[-1] == path else ","))
    cfile.write("enum_end (NegCurve)\n")
    header.write("};\n")
