import logging
from pprint import pformat

import colour
import numpy as np

from .color import Colour, CurvesTrace, SpectralTriple, Tricolour, rgb2sp, sp2rgb
from .color_data import CIERGB, shp47

logger = logging.getLogger(__name__)


class ColorSensitivity:
    sens_layer: SpectralTriple | None

    def __init__(self) -> None:
        self.sens_layer = None

    def get_sensitivity(self, sensitivity: CurvesTrace) -> dict[str, list[float]]:
        self.sens_layer = {
            x: colour.SpectralDistribution(
                [10**x for x in sensitivity[x]["sensitivity"]],
                sensitivity[x]["wavelength"],
                name=x + " dye sensitivity",
                interpolator=colour.LinearInterpolator,
                interpolator_kwargs={},
                extrapolator=colour.Extrapolator,
                extrapolator_kwargs={},
            ).align(shp47)
            for x in sensitivity
        }

        # CMY with yellow mask on negative, RGB on positive print
        in_r = Colour.from_xyy(np.array([0.381263, 0.445763, 0.194181]))
        in_g = Colour.from_xyy(np.array([0.488243, 0.313020, 0.155637]))
        in_b = Colour.from_xyy(np.array([0.569528, 0.399893, 0.232698]))
        in_xyz = Tricolour(in_r, in_g, in_b)
        logger.debug(
            "CMY input for conversion: in_xyz.a.xyz=%s\n"
            "in_xyz.b.xyz=%s\n"
            "in_xyz.c.xyz=%s",
            in_xyz.a.xyz,
            in_xyz.b.xyz,
            in_xyz.c.xyz,
        )

        in_rgb_list = [
            [sp2rgb(rgb2sp(xyz.xyz), cmfs=sensitivity[layer]) for layer in sensitivity]
            for xyz in [in_xyz.a, in_xyz.b, in_xyz.c]
        ]
        in_rgb = Tricolour.from_matrix(in_rgb_list)
        x = in_rgb / in_xyz
        x *= 60
        logger.debug("Sensitivity matrix: %s", x)
        x = np.matmul(x, CIERGB)
        x = x.T

        # Invert the primaries matrix
        xyz_sens = {
            "rsens": x[0,],
            "gsens": x[1,],
            "bsens": x[2,],
        }

        xyz_sens_inv = np.matrix(
            [xyz_sens["rsens"], xyz_sens["gsens"], xyz_sens["bsens"]],
        )
        xyz_sens = {
            "rsens": list(xyz_sens_inv[:, 0].A1),
            "gsens": list(xyz_sens_inv[:, 1].A1),
            "bsens": list(xyz_sens_inv[:, 2].A1),
        }
        logger.debug("Inverted XYZ sensitivity: %s", pformat(xyz_sens))
        return xyz_sens
