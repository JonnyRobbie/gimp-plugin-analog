import colour

from .color import CurvesTrace, SpectralTriple, Tricolour


def color_plot(
    dye: SpectralTriple,
    density: CurvesTrace,
    xyz: Tricolour,
    sens_layer: SpectralTriple,
    _sensitivity: CurvesTrace,
) -> None:
    colour.plotting.plot_multi_sds(
        [dye["cyan"], dye["magenta"], dye["yellow"]],
        use_sds_colours=True,
    )
    dye_xyz = {x: y / 100 for x, y in zip(density, (xyz.a.xyz, xyz.b.xyz, xyz.c.xyz))}
    dye_xyz["red"] = dye_xyz["yellow"] * dye_xyz["magenta"]
    dye_xyz["green"] = dye_xyz["cyan"] * dye_xyz["yellow"]
    dye_xyz["blue"] = dye_xyz["magenta"] * dye_xyz["cyan"]
    dye_xyz["black"] = dye_xyz["cyan"] * dye_xyz["magenta"] * dye_xyz["yellow"]
    swatches = [
        colour.plotting.ColourSwatch(name=x, RGB=colour.XYZ_to_sRGB(dye_xyz[x]))
        for x in ["red", "cyan", "green", "magenta", "blue", "yellow", "black"]
    ]

    colour.plotting.plot_multi_colour_swatches(swatches)
    colour.plotting.plot_sds_in_chromaticity_diagram_CIE1931(
        [dye["cyan"], dye["magenta"], dye["yellow"]],
    )

    colour.plotting.plot_multi_sds(
        [sens_layer["red"], sens_layer["green"], sens_layer["blue"]],
        use_sds_colours=True,
    )
