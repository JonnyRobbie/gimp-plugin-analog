import logging

import numpy as np
import pandas as pd
import plotly.graph_objects as go

from html_templates import mg_plot
from paper.color import Colour, Tricolour
from paper.paper_data import PaperData

logger = logging.getLogger(__name__)


class ColTrace:
    sensitivity: dict[str, pd.Series]
    curves: dict[str, pd.Series]
    density: dict[str, pd.Series]

    def __init__(
        self,
        sensitivity: dict[str, pd.Series],
        curves: dict[str, pd.Series],
        density: dict[str, pd.Series],
    ) -> None:
        self.sensitivity = sensitivity
        self.curves = curves
        self.density = density

    def to_data(self) -> PaperData:
        return PaperData(
            sensitivity=np.array(((0.0, 0.0, 0.0), (0.0, shift, 1.0), (0.0, 1.0, 1.0))),
            red=curves["red"],
            green=curves["green"],
            blue=curves["blue"],
            dye=Tricolour(
                Colour(0.1, 0.1, 0.1),
                Colour(0.1, 0.1, 0.1),
                Colour(0.1, 0.1, 0.1),
            ),
        )

    def plot(self, trace_name: str) -> str:
        dh = go.Figure()
        return mg_plot(trace_name, dh)
