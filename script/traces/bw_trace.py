import numpy as np
import pandas as pd
import plotly.graph_objects as go

from html_templates import bw_report
from paper.color import Colour, Tricolour
from paper.paper_data import PaperData


class BWTrace:
    trace: pd.Series

    def __init__(self, trace: pd.Series) -> None:
        self.trace = trace

    def to_data(self) -> PaperData:
        sensitivity = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 1]])
        red = pd.Series((0, 0), index=(1, 5))
        green = pd.Series((0, 0), index=(1, 5))
        blue = self.trace
        dye = Tricolour(Colour(1, 1, 1), Colour(1, 1, 1), Colour(0.1, 0.1, 0.1))

        return PaperData(
            sensitivity=sensitivity,
            red=red,
            green=green,
            blue=blue,
            dye=dye,
        )

    def plot(self, trace_name: str) -> str:
        dh = go.Figure()
        return bw_report(trace_name, dh)
