# Negative darkroom gimp plug-in and gegl operation

## gegl operation

`negative-darkroom` is a gegl operation which tries to simulate behaviour of
photographic darkroom papers when inverting scanned film negatives.

![screenshot of the operation in action](media/screenshot.jpg)

### build

Needs `gegl` for proper build. (`libgegl-dev` on \*buntu distros)

    make

### install

This operation got merged straight to the `gegl` tree. The generating scripts
are still hosted on this repo.
Copy (or symlink) `bin/negative-darkroom.so` to `/usr/lib/gegl-0.4` (or distro
equivalent) or preload with LD_PRELOAD.

### AUR

AUR version is https://aur.archlinux.org/packages/gegl-operation-negative-darkroom-git/

## Usage

 - **Aux Input** Dodge/burn layer. 50% gray makes no change in the image exposure,
darker color represents burn and results in a darker image, while lighter color
represents dodge and results in lighter image.

 - **Characteristic curve** Represents particular darkroom paper characteristic
curve. In case of a color negative paper, it also represents it's spectrum sensitivity
and spectrum density.

 - **Exposure** Overall exposure. The slider retains the general notion that right means
light and left means dark.

 - **Filter cyan/magenta/yellow** Color filter for color balance of color negatives.

 - **Clip base+fog** Real photographic papers may display some minimal density
even without any exposure and this plugin simulates even that. In most cases it would be impractical.
This option clips base+fog to pure white.

 - **Density boost** Real typical photographic paper has maximum density of around
2.2, which means that even at maximum exposure, the paper blocks only about 99.3695% of light.
This hovever still results in sRGB values of around 20 (out of 255). This option artificially
boosts the maximum density.

 - **Dodge/burn multiplier** Adjusts the intensity of the dodge/burn layer.

 - **Enable preflashing, Red/Green/Blue preflash** Simulates paper preflash.
This usually has the effect of lowering the contrast or controlling the color balance
of highlight/shadows separately.

### Custom DH Curves

You can create a pull request with custom DH curves if you manage to find/create your own data.
Create an appropriate subdirectory in datasheet/ (see existing data as an example) and add them to a Makefile.

## Donation

If you feel like I've done a good job, I accept bitcoin donations:

[bc1qnfqcu6mj58lumvrv2snl9ytlz7hf2fnjckuaph](bitcoin:bc1qnfqcu6mj58lumvrv2snl9ytlz7hf2fnjckuaph)

