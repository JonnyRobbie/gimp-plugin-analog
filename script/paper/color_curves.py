import itertools

Curves = dict[str, dict[str, list[float]]]


def color_curves(dh: Curves) -> tuple[dict[str, list[float]], dict[str, int]]:
    dh_keys = {"red": "r", "green": "g", "blue": "b"}
    dh_keys2 = {"exposure": "x", "density": "y"}
    dh_out = {
        dh_keys[x] + dh_keys2[y]: dh[x][y]
        for x, y in itertools.product(["red", "green", "blue"], ["exposure", "density"])
    }
    dh_n = {dh_keys[x] + "n": len(dh[x]["exposure"]) for x in ["red", "green", "blue"]}
    return dh_out, dh_n
