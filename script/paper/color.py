from __future__ import annotations

import logging
from typing import TYPE_CHECKING, Self

import colour
import numpy as np
import scipy as sp

from .color_data import sd_d50

if TYPE_CHECKING:
    from collections.abc import Iterator

logger = logging.getLogger(__name__)


CurvesTrace = dict[str, dict[str, list[float]]]
CurvesTraceList = dict[str, list[list[float]]]
SpectralTriple = dict[str, colour.SpectralDistribution]


class StrColor(str):
    list_rgb: tuple[str, str, str] = ("red", "green", "blue")
    list_cmy: tuple[str, str, str] = ("cyan", "magenta", "yellow")
    _map: dict[str, str]
    __slots__ = ()

    def __new__(cls, color: str) -> Self:
        if color not in cls.list_rgb + cls.list_cmy:
            msg = f"Invalid color name '{color}'"
            raise ValueError(msg)
        cls._map = dict(
            zip(
                cls.list_rgb + cls.list_cmy,
                cls.list_cmy + cls.list_rgb,
                strict=False,
            ),
        )
        return str.__new__(cls, color)

    def __invert__(self) -> StrColor:
        return StrColor(self._map[self])


list_rgb = [StrColor(c) for c in StrColor.list_rgb]
list_cmy = [~c for c in list_rgb]


class Colour:
    x: float
    y: float
    z: float
    rgb_matrix: np.ndarray | None

    def __init__(self, x: float, y: float, z: float) -> None:
        self.rgb_matrix = None
        self.x = x
        self.y = y
        self.z = z

    @property
    def xyz(self) -> np.ndarray:
        return np.array([self.x, self.y, self.z])

    @xyz.setter
    def xyz(self, xyz: np.ndarray) -> None:
        self.x = xyz[0]
        self.y = xyz[1]
        self.z = xyz[2]

    @property
    def xyy(self) -> np.ndarray:
        return np.array(
            [
                self.x / (self.x + self.y + self.z),
                self.y / (self.x + self.y + self.z),
                self.y,
            ],
        )

    @xyy.setter
    def xyy(self, xyy: np.ndarray) -> None:
        self.x = xyy[2] * xyy[0] / xyy[1]
        self.y = xyy[2]
        self.z = xyy[2] * (1 - xyy[0] - xyy[1]) / xyy[1]

    @property
    def rgb(self) -> np.ndarray:
        if self.rgb_matrix is None:
            msg = "The RGB matrix was not yet defined"
            raise AttributeError(msg)
        inv_rgb_matrix = np.linalg.inv(self.rgb_matrix)
        return np.matmul(inv_rgb_matrix, self.xyz)

    @rgb.setter
    def rgb(self, rgb: np.ndarray) -> None:
        if self.rgb_matrix is None:
            msg = "The RGB matrix was not yet defined"
            raise AttributeError(msg)
        xyz = np.matmul(self.rgb_matrix, rgb)
        self.xyz = xyz

    @property
    def srgb(self) -> str:
        srgb = np.clip(colour.XYZ_to_sRGB([self.x, self.y, self.z]), 0, 1)
        return "#" + "".join([f"{int(c * 255):02x}" for c in srgb])

    @property
    def density(self) -> np.ndarray:
        return np.log10(1 / self.xyz)

    @classmethod
    def from_xyz(cls, xyz: np.ndarray) -> Colour:
        return Colour(xyz[0], xyz[1], xyz[2])

    @classmethod
    def from_xyy(cls, xyy: np.ndarray) -> Colour:
        colour = Colour(0, 0, 0)
        colour.xyy = xyy
        return colour

    def __repr__(self) -> str:
        return f"Colour X={self.x:.4f} Y={self.y:.4f} Z={self.z:.4f} {self.srgb}"


class Tricolour:
    a: Colour
    b: Colour
    c: Colour

    def __init__(self, a: Colour, b: Colour, c: Colour) -> None:
        self.a = a
        self.b = b
        self.c = c

    @classmethod
    def from_matrix(cls, m) -> Tricolour:
        a = Colour(m[0][0], m[0][1], m[0][2])
        b = Colour(m[1][0], m[1][1], m[1][2])
        c = Colour(m[2][0], m[2][1], m[2][2])
        return Tricolour(a, b, c)

    def __truediv__(self, second: Tricolour) -> np.ndarray:
        # TODO(marek): check return type
        zeros = [0, 0, 0]
        m = np.vstack(
            [
                np.matrix(
                    [
                        np.concatenate([second.a.xyz, zeros, zeros]),
                        np.concatenate([zeros, second.a.xyz, zeros]),
                        np.concatenate([zeros, zeros, second.a.xyz]),
                    ],
                ),
                np.matrix(
                    [
                        np.concatenate([second.b.xyz, zeros, zeros]),
                        np.concatenate([zeros, second.b.xyz, zeros]),
                        np.concatenate([zeros, zeros, second.b.xyz]),
                    ],
                ),
                np.matrix(
                    [
                        np.concatenate([second.c.xyz, zeros, zeros]),
                        np.concatenate([zeros, second.c.xyz, zeros]),
                        np.concatenate([zeros, zeros, second.c.xyz]),
                    ],
                ),
            ],
        )
        b = np.concatenate([self.a.xyz, self.b.xyz, self.c.xyz])
        return np.reshape(np.linalg.solve(m, b), (3, 3))

    def __repr__(self) -> str:
        return f"Triple\nA: {self.a}\nB: {self.b}\nC: {self.c}"

    def __iter__(self) -> Iterator[Colour]:
        return iter([self.a, self.b, self.c])


def rgb2sp(rgb: np.ndarray) -> colour.SpectralDistribution:
    # Fuji eterna negative emulsion densities
    rw = [
        379.080118694362,
        402.225519287834,
        435.163204747775,
        451.186943620178,
        465.430267062315,
        490.356083086054,
        571.810089020772,
        699.554896142433,
    ]
    rs = [0.2, 0.298, 0.644, 0.706, 0.636, 0.374, 0.028, 0.014]
    gw = [
        379.525222551929,
        445.845697329377,
        534.421364985163,
        551.780415430267,
        592.284866468843,
        649.258160237389,
    ]
    gs = [0.156, 0.182, 0.868, 0.902, 0.2, 0.084]
    bw = [
        487.685459940653,
        548.219584569733,
        635.905044510386,
        656.379821958457,
        674.629080118694,
        679.080118694362,
    ]
    bs = [0.14, 0.156, 1.04, 1.114, 1.048, 0.556]
    wavelenghts = np.array(range(400, 760, 10))

    def _rgb2sp(x: np.ndarray) -> np.ndarray:
        # out = np.ones(wavelenghts.shape[0])
        out = sd_d50.values / 100

        rsn = 1 / 10 ** (x[0] * np.array(rs))
        out *= np.interp(wavelenghts, rw, rsn)

        gsn = 1 / 10 ** (x[1] * np.array(gs))
        out *= np.interp(wavelenghts, gw, gsn)

        bsn = 1 / 10 ** (x[2] * np.array(bs))
        out *= np.interp(wavelenghts, bw, bsn)
        return out

    def fun(x: np.ndarray) -> float:
        spec = colour.SpectralDistribution(
            dict(zip(wavelenghts, _rgb2sp(x), strict=False)),
        )

        out = colour.sd_to_XYZ(spec) / 100

        return ((np.array(rgb) - out) ** 2).mean()

    logger.debug("Optimizing spectral distribution")
    opt = sp.optimize.minimize(
        fun,
        np.array([1, 1, 1]),
        method="COBYLA",
        options={"maxiter": 10000},
    )
    return colour.SpectralDistribution(
        dict(zip(wavelenghts, _rgb2sp(opt.x), strict=False)),
    )


def sp2rgb(
    spectrum: colour.SpectralDistribution,
    cmfs: dict[str, list[float]],
) -> float:
    logger.debug("Convert from xyz to SD")
    # deltas of the paper sensitivity spectrum wavelength datapoints
    wav_delta = np.array(
        [
            x - y
            for x, y in zip(
                cmfs["wavelength"][1:],
                cmfs["wavelength"][:-1],
                strict=False,
            )
        ],
    )

    # midpoint of the paper sensitivity wavelength datapoints
    wav_mid = np.array(
        [
            (x + y) / 2
            for x, y in zip(
                cmfs["wavelength"][1:],
                cmfs["wavelength"][:-1],
                strict=False,
            )
        ],
    )

    # color patch spectrum intensity interpolated to paper data wavelength midpoints
    # patchInterps = 10**np.interp(wavMid, spectrum.wavelengths, spectrum.values)
    patch_interps = np.interp(wav_mid, spectrum.wavelengths, spectrum.values)

    # midpoints of the paper sensitivity intensity datapoints
    spectr_mid: np.ndarray = 10 ** np.array(
        [
            (x + y) / 2
            for x, y in zip(
                cmfs["sensitivity"][1:],
                cmfs["sensitivity"][:-1],
                strict=False,
            )
        ],
    )

    spectr_mid /= 10

    out = sum(wav_delta * spectr_mid * patch_interps) / 1000
    logger.debug("Sum: %s", out)

    return out
