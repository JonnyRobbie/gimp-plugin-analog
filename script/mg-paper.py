#!/usr/bin/env python3

import argparse
import json
import logging
from pathlib import Path

import pandas as pd
from plotly.io._base_renderers import open_html_in_browser

from traces.mg_trace import MGTrace

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def main(output: str, directory: str, *, plot: bool, report: bool) -> None:
    curves = {
        grade: pd.read_csv(Path(directory) / f"dh-{grade}.csv", index_col=0)["density"]
        for grade in ["00", "0", "1", "2", "3", "4", "5"]
        if (Path(directory) / f"dh-{grade}.csv").exists()
    }
    traces = MGTrace(traces=curves)
    if plot:
        open_html_in_browser(traces.plot(directory))
    paper_data = traces.to_data()
    if report:
        open_html_in_browser(paper_data.report("mg", directory))
    paper_dict = paper_data.to_dict()
    with Path(output).open("w") as json_file:
        json.dump(paper_dict, json_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process multigrade DH curves.")
    parser.add_argument("-o", help="Output paper description json")
    parser.add_argument("--plot", help="Plot loaded traces.", action="store_true")
    parser.add_argument(
        "--report",
        help="Compile a report with computed paper data.",
        action="store_true",
    )
    parser.add_argument("dir", help="Directory with the dh-xx.csv files.")
    args = parser.parse_args()

    logging.info("Processing %s", args.dir)
    main(args.o, args.dir, plot=args.plot, report=args.report)
