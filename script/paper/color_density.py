import logging

import colour
import numpy as np
import scipy as sp

from paper.color import StrColor

from .color import Colour, CurvesTrace, SpectralTriple, Tricolour, list_cmy, list_rgb
from .color_data import sd_a, shp475, status_a

logger = logging.getLogger(__name__)


class ColorDensity:
    cmy_d1: Tricolour | None
    dye: SpectralTriple | None

    def __init__(self) -> None:
        self.cmy_d1 = None
        self.dye = None

    def denormalize_density(self, density: CurvesTrace, color: StrColor) -> float:
        dens_inter = colour.Extrapolator(
            colour.LinearInterpolator(
                x=density[~color]["wavelength"],
                y=density[~color]["density"],
            ),
            method="constant",
            right=0,
        )(range(400, 760, 10))

        def dens_diff(x: list[float]) -> float:
            dens_denorm = 1 / 10 ** (x[0] * dens_inter)
            dens_denorm_a = dens_denorm * sd_a.values
            dens_a_filtered = dens_denorm_a * status_a[color]
            status_a_ill_a = status_a[color] * sd_a.values
            total_light_color = np.sum(dens_a_filtered)
            total_light_white = np.sum(status_a_ill_a)
            return ((total_light_color / total_light_white) - 0.1) ** 2

        opt = sp.optimize.minimize(dens_diff, np.array([1]), method="COBYLA")
        logger.debug("Denormalization factor: %s", {opt.x[0]})

        fudge_factor = {"red": 1, "green": 1, "blue": 1}
        return 1 / 10 ** (opt.x[0] * dens_inter * fudge_factor[color])

    def get_density(self, density: CurvesTrace) -> Tricolour:
        logger.debug("Getting density for color")

        t = {~c: self.denormalize_density(density, c) for c in list_rgb}

        self.dye = {
            x: colour.SpectralDistribution(
                t[x],
                range(400, 760, 10),
                name=x + " dye density",
                interpolator=colour.LinearInterpolator,
                interpolator_kwargs={},
                extrapolator=colour.Extrapolator,
                extrapolator_kwargs={},
            ).align(shp475)
            for x in list_cmy
        }

        self.cmy_d1 = Tricolour(
            *tuple(
                Colour.from_xyz(colour.sd_to_XYZ(self.dye[x]) / 100) for x in self.dye
            ),
        )
        return self.cmy_d1
